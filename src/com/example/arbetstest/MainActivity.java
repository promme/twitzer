package com.example.arbetstest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends Activity {
	ListView Tweetlist;
	String[] TweetArray = { "no tweets" };
	ArrayAdapter<String> tweetAdapter;
	Context context;
	String url = "http://176.58.126.236/twitzer/";
	ArrayList<String> tweetArrayList = new ArrayList<String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		context = this;
		Tweetlist = (ListView) findViewById(R.id.tweetList);
		tweetAdapter = new ArrayAdapter<String>(this, R.layout.tweet_list_item,
				tweetArrayList);
		Tweetlist.setAdapter(tweetAdapter);

		TweetListTask loaderTask = new TweetListTask();
		loaderTask.execute();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	public class TweetListTask extends AsyncTask<Void, Void, Void> {

		ProgressDialog dialog;

		@Override
		protected void onPreExecute() {

			dialog = new ProgressDialog(context);
			dialog.setTitle("Loading Tweets");
			dialog.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			HttpClient client = new DefaultHttpClient();
			HttpGet getRequest = new HttpGet(url);
			
			try {
				HttpResponse response = client.execute(getRequest);
				StatusLine statusline = response.getStatusLine();
				int statusCode = statusline.getStatusCode();
				if (statusCode!=200)
				{
					return null;
				}
				
				InputStream jsonStream = response.getEntity().getContent();
				
				BufferedReader reader = new BufferedReader(new InputStreamReader(jsonStream));
				
				StringBuilder builder = new StringBuilder();
				
				String line;
				while((line = reader.readLine()) != null){
					builder.append(line);
				}
				
				String jsonData = builder.toString();
				JSONArray json = new JSONArray(jsonData);
				Log.i("tweetJsonData", jsonData);
				
				for(int i = 0; i < json.length(); i++){
					JSONObject tweet = json.getJSONObject(i);
					tweetArrayList.add(tweet.getString("user") + ": " +tweet.getString("text"));
				}
				
				
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			dialog.dismiss();
			tweetAdapter.notifyDataSetChanged();
			super.onPostExecute(result);
		}

	}

}
